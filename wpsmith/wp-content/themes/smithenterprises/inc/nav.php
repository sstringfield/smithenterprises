<div id="nav_wrapper">
	<div class="container">
		
		<span id="logo"><a href="/">Smith Enterprises, LLC</a></span>

		<div id="nav">
			<?php $defaults = array ( 'menu' => 'Main Navigation Menu' ); ?>
			<?php wp_nav_menu( $dafaults ); ?>
		</div><!-- #nav -->

	</div><!-- .container -->
</div><!-- #nav_wrapper -->

<?php if ($pagename != '') { ?>
	<div id="sub_nav_bg">
		<?php
			if($post->post_parent)
			  	$children = wp_list_pages("title_li=&sort_column=menu_order&child_of=".$post->post_parent."&echo=0");
			else
			  	$children = wp_list_pages("title_li=&sort_column=menu_order&child_of=".$post->ID."&echo=0");
			if ($children) { ?>
				<div class="container">
				  	<ul id="subnav">
				  		<?php echo $children; ?>
				  	</ul>
			  	</div>
		<?php } ?>
	</div><!-- #subnav -->
<?php } ?>