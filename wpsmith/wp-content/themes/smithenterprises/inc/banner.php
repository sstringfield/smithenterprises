<div id="banner"> 
	<div class="container">
		<div id="appointments">
			<img src="<?php bloginfo('template_directory') ?>/images/banner_appointments.png" alt="Schedule an Appointment" />
			<p class="btn_banner">Schedule Appointment</p>
		</div><!-- #appointments -->

		<div id="tracktaxes">
			<img src="<?php bloginfo('template_directory') ?>/images/banner_tracktaxes.png" alt="Track Your Taxes" />
			<p class="btn_banner"><a class="fancybox" href="#tracklinks">Track Your Taxes</a></p>
		</div><!-- #tracktaxes -->

		<div id="tracklinks">
			<?php $args = array('post_type' => 'sidebar_items', p => '53'); ?>
			<?php $cont = new WP_Query($args); ?>
			<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
				<h2><?php the_title() ?></h2>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>

		<div id="downloadforms">
			<img src="<?php bloginfo('template_directory') ?>/images/banner_downloadforms.png" alt="Download Tax Forms" />
			<p class="btn_banner">Download Tax Forms</p>
		</div><!-- #downloadforms -->
	</div>
</div><!-- #banner -->