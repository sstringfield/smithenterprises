<?php get_header(); ?>

<div id="content" class="container">
	<div id="main">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>

	<div id="aside">
		<?php get_sidebar(); ?>
	</div>
</div><!-- #content -->

<?php get_footer(); ?>