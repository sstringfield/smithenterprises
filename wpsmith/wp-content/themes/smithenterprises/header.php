<?php $pagename = get_query_var('pagename'); ?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="">
	<!-- <meta name="viewport" content="width=device-width"> -->

	<title>
		<?php
		// Returns the title based on the type of page being viewed -- jeebus this is a mess.
		if (is_single()){ single_post_title(); echo ' | ';  bloginfo( 'name' );
			} elseif (is_home() || is_front_page()){ bloginfo( 'name' ); if(get_bloginfo( 'description' )){ echo ' | ' ; bloginfo( 'description' ); }
			} elseif ( is_page() ){ single_post_title( '' ); echo ' | '; bloginfo( 'name' );
			} elseif ( is_search() ){ printf( __( 'Search results for "%s"', '' ), get_search_query() ); echo ' | '; bloginfo( 'name' );
			} elseif ( is_404() ){ _e( 'Not Found', '' ); echo ' | '; bloginfo( 'name' );
			} else { wp_title(''); echo ' | '; bloginfo( 'name' ); 
		}
	?>
	</title>

	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/js/libs/fancybox/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<script src="<?php bloginfo('template_directory') ?>/js/libs/modernizr-2.6.1.min.js"></script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="header">
	
	<?php include 'inc/nav.php' ?>

	<?php if ($pagename == '') { ?>
		<?php include 'inc/banner.php'; ?>

		<div id="banner_action_area">
			
			<?php $args = array('post_type' => 'text_snippets', p => '8'); ?>
			<?php $cont = new WP_Query($args); ?>
			<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>

			<div class="container">
				<?php include 'inc/email_list.php'; ?>
			</div>
		</div><!-- #emailsignup -->
	<?php } ?>
</div><!-- #header -->