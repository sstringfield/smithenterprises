<div id="fat_footer">
	<div class="container">
		<div class="address">
			<h2>Location</h2>
			<?php $args = array('post_type' => 'text_snippets', p => '9'); ?>
			<?php $cont = new WP_Query($args); ?>
			<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</div>

		<!-- Contact Form -->
		<?php $args = array('post_type' => 'text_snippets', p => '131'); ?>
		<?php $cont = new WP_Query($args); ?>
		<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div><!-- #fat_footer -->

<div id="skinny_footer">
	<div class="container">
		<p class="footer_info">Smith Enterprises, LLC<span class="copyright"> | Copyright &copy; <?php echo date('Y'); ?></span></p>
	</div>
</div><!-- #footer -->

<script src="<?php bloginfo('template_directory') ?>/js/libs/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?php bloginfo('template_directory') ?>/js/main.js"></script>

<script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
</script>
	
<?php wp_footer(); ?>

</body>
</html>