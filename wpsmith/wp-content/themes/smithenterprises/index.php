<?php get_header(); ?>

<div id="content" class="container">
	<div class="col1">
		<?php $args = array('post_type' => 'homepage_items', p => '4'); ?>
		<?php $cont = new WP_Query($args); ?>
		<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
			<h2><?php the_title() ?></h2>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>

	<div class="col1">
		<?php $args = array('post_type' => 'homepage_items', p => '5'); ?>
		<?php $cont = new WP_Query($args); ?>
		<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
			<h2><?php the_title() ?></h2>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>

	<div id="newsletter" class="col1">
		<?php $args = array('post_type' => 'homepage_items', p => '6'); ?>
		<?php $cont = new WP_Query($args); ?>
		<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
			<h2><?php the_title() ?></h2>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>

	<div id="pdf_downloads" class="col2">
		<img src="<?php bloginfo('template_directory') ?>/images/icon_pdf.png" alt="Download Smith Enterprises Expense Forms">
		<?php $args = array('post_type' => 'homepage_items', p => '7'); ?>
		<?php $cont = new WP_Query($args); ?>
		<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
			<h3><?php the_title() ?></h3>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</div>
</div><!-- #content -->

<?php get_footer(); ?>