(function($) {

	var Main = {
		init: function() {
			this.domEnhancements();
			this.trackTaxes();
		},

		trackTaxes: function() {
			var tracklinks = $('#tracklinks');

			$('.fancybox').fancybox();
		},

		domEnhancements: function() {
			// Stripe Table
			$('tr:odd').addClass('odd');
		}
	}

	$('document').ready(function() { Main.init(); });
	

})(jQuery);