<?php /* Template Name: Tax Center */ ?>

<?php get_header(); ?>

<div id="content" class="container">
	<div id="main">
		<?php $args = array('post_type' => 'tax_center_links'); ?>
		<?php $cont = new WP_Query($args); ?>
		<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
			<div class="tax_links">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div>
		<?php endwhile; ?>
	</div>

	<div id="aside">
		<?php get_sidebar(); ?>
	</div>
</div><!-- #content -->

<?php get_footer(); ?>