<?php 

function register_menus() {
	if ( function_exists('register_nav_menus') ) {
		register_nav_menus(
			array(
				'main_nav' => 'Main Navigation Menu'
			)
		);
	}
}

function add_post_type($name, $args = array()) {
	add_action('init', function() use($name, $args) {
		$upper_case_name = ucwords($name);
		$name = strtolower(str_replace(' ', '_', $name));

		$args = array_merge(
			array(
			'public' => true,
			'label' => "$upper_case_name",
			'labels' => array('add_new_item' => "Add New $upper_case_name Item"),
			),
			$args
		);
		register_post_type($name, $args);
	});
}

add_post_type('homepage items');
add_post_type('text snippets');
add_post_type('tax center links');
add_post_type('sidebar items');
add_post_type('newsletter');

add_action( 'init', 'register_menus' );

add_theme_support( ‘menus’ );

/* remove_filter ('the_content', 'wpautop'); */

?>