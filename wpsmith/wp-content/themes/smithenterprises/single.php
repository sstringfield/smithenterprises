<?php get_header(); ?>

<div id="content" class="container">
	<div id="main">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h2 id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>
			<p class="blogdate"> <?php the_time('F jS') ?>, <?php the_time('Y') ?></p>

			<?php the_content(__('Read more'));?>
			
			<p class="blogcategory">Categories: <?php the_category(' &bull; '); ?></p>
			<?php edit_post_link(__('<strong>Edit</strong>'));?>
			<?php comments_template(); // Get comments.php template ?>

		<?php endwhile; else: ?>
			<p> <?php _e('Sorry, no posts matched your criteria.'); ?> </p>
		<?php endif; ?>
	</div>

	<div id="aside">
		<?php get_sidebar(); ?>
	</div>
</div><!-- #content -->

<?php get_footer(); ?>