<?php $args = array( 'post_type' => 'sidebar_items' ); ?>
<?php $cont = new WP_Query( $args ); ?>

<?php while ( $cont -> have_posts() ) : $cont -> the_post(); ?>
	<div class="sidebar_item">
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>
	</div>
<?php endwhile; ?>