<?php /* Template Name: Blog */ ?>

<?php get_header(); ?>

<div id="content" class="container">
	<div id="main">
		<?php
		global $post;
		$posts = get_posts();
		foreach( $posts as $post ) : setup_postdata($post); ?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php the_content(); ?>
		<?php endforeach; ?>
	</div>

	<div id="aside">
		<?php get_sidebar(); ?>
	</div>
</div><!-- #content -->

<?php get_footer(); ?>